# py-docker
Simple Dockerfile to build and deploy python apps inside containers.

I use this as a base image for my private projects. Feel free to use as is ..

This is based on `FROM python:3.11-slim`. Images are build and pushed to
docker-hub for amd64 & arm64 platforms.

> Check `requirements.txt` for python packages which are installed.